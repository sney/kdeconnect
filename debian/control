Source: kdeconnect
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
Build-Depends: cmake (>= 3.0~),
               debhelper-compat (= 13),
               dh-sequence-kf5,
               extra-cmake-modules (>= 5.96.0~),
               gettext,
               kirigami2-dev (>= 5.96.0~),
               kpeople-vcard,
               libfakekey-dev,
               libkf5configwidgets-dev (>= 5.96.0~),
               libkf5dbusaddons-dev (>= 5.96.0~),
               libkf5doctools-dev (>= 5.96.0~),
               libkf5guiaddons-dev (>= 5.96.0~),
               libkf5i18n-dev (>= 5.96.0~),
               libkf5iconthemes-dev (>= 5.96.0~),
               libkf5kcmutils-dev (>= 5.96.0~),
               libkf5kio-dev (>= 5.96.0~),
               libkf5notifications-dev (>= 5.96.0~),
               libkf5package-dev (>= 5.71.0~),
               libkf5people-dev (>= 5.96.0~),
               libkf5pulseaudioqt-dev,
               libkf5qqc2desktopstyle-dev (>= 5.96.0~),
               libkf5service-dev (>= 5.96.0~),
               libkf5solid-dev (>= 5.96.0~),
               libkf5wayland-dev (>= 4:5.96.0~),
               libkf5windowsystem-dev (>= 5.96.0~),
               libqca-qt5-2-dev (>= 2.1.0~),
               libqt5waylandclient5-dev (>= 5.10.0~),
               libqt5x11extras5-dev (>= 5.15.2~),
               libwayland-dev (>= 1.9~),
               libx11-dev,
               libxtst-dev,
               pkg-config,
               plasma-wayland-protocols,
               qtbase5-dev (>= 5.15.2~),
               qtbase5-private-dev (>= 5.10.0~),
               qtdeclarative5-dev (>= 5.15.2~),
               qtmultimedia5-dev (>= 5.15.2~),
               qtquickcontrols2-5-dev (>= 5.15.2~),
               qtwayland5-dev-tools,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://kdeconnect.kde.org
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kdeconnect.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kdeconnect

Package: kdeconnect
Architecture: any
Depends: fuse3 (>= 3),
         kpeople-vcard,
         libqca-qt5-2-plugins,
         plasma-framework,
         qml-module-org-kde-kirigami2,
         qml-module-org-kde-kquickcontrolsaddons,
         qml-module-org-kde-people,
         qml-module-qt-labs-platform,
         qml-module-qtgraphicaleffects,
         qml-module-qtmultimedia,
         qml-module-qtqml,
         qml-module-qtquick-controls2,
         qml-module-qtquick-dialogs,
         qml-module-qtquick-layouts,
         qml-module-qtquick-particles2,
         qml-module-qtquick-window2,
         qml-module-qtquick2,
         sshfs (>= 3),
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: fuse (<< 3),
Description: connect smartphones to your desktop devices
 Tool to integrate your smartphone, tablet, and desktop devices.
 Remote-control, share files, synchronize notifications, and more!
 At the moment it only supports Android-based mobile devices. Linux desktop
 devices are well-supported, with ports available for other operating systems.

Package: nautilus-kdeconnect
Architecture: all
Depends: kdeconnect, nautilus, python3-nautilus, ${misc:Depends},
Enhances: nautilus,
Breaks: kdeconnect (<< 20.12),
Replaces: kdeconnect (<< 20.12),
Description: KDE Connect integration for Nautilus
 Extension providing integration to send files to KDE Connect compatible
 devices directly from within Nautilus.
